<?php

if (!defined('\Project\Tools\Modules\IS_START')) {
    include_once(dirname(__DIR__) . '/project.tools/modules/install.php');
}

use Bitrix\Main\Localization\Loc,
    Project\Tools\Modules;

IncludeModuleLangFile(__FILE__);

class project_upload extends CModule {

    public $MODULE_ID = 'project.upload';

    use Modules\Install;

    function __construct() {
        $this->setParam(__DIR__, 'PROJECT_UPLOAD');
        $this->MODULE_NAME = Loc::getMessage('PROJECT_UPLOAD_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_UPLOAD_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_UPLOAD_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('PROJECT_UPLOAD_PARTNER_URI');
    }

    /*
     * InstallAgent
     */

    public function InstallAgent() {
//        Modules\Utility::agentAdd(
//                '\Project\Upload\Agent\Router::process();', // имя функции
//                $this->MODULE_ID, // идентификатор модуля
//                'Y', // агент не критичен к кол-ву запусков
//                10, // интервал запуска - 1 сутки
//                '', // дата первой проверки - текущее
//                'Y', // агент активен
//                '', // дата первого запуска - текущее
//                30);
    }

    public function UnInstallAgent() {
//        CAgent::RemoveAgent('\Project\Upload\Agent\Router::process();', $this->MODULE_ID);
    }

}
