<?php

namespace Project\Upload\Helper;

use Project\Upload\Config,
    Project\Upload\Settings;

class Runtime {

    static public function isStart() {
//        return true;
        $is = Settings::get(__CLASS__);
        if ($is and ( $is + Config::AGENT_TIMEOUT) > time()) {
            return false;
        }
        self::next();
        return true;
    }

    static private function next() {
        Settings::set(__CLASS__, time());
    }

    static public function stop() {
        Settings::clear(__CLASS__);
    }

}
