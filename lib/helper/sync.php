<?php

namespace Project\Upload\Helper;

use Project\Upload\Model;

class Sync {

    static public function update($key, $value) {
        Model\Iblock4Table::update(array('IMPORT_TYPE' => $key), array(
            'IMPORT_STATUS' => $value
        ));
        Model\Iblock5Table::update(array('IMPORT_TYPE' => $key), array(
            'IMPORT_STATUS' => $value
        ));
        Model\Iblock6Table::update(array('IMPORT_TYPE' => $key), array(
            'IMPORT_STATUS' => $value
        ));
        Model\Iblock7Table::update(array('IMPORT_TYPE' => $key), array(
            'IMPORT_STATUS' => $value
        ));
    }

}
