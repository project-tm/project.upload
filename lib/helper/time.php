<?php

namespace Project\Upload\Helper;

use Project\Upload\Config,
    Project\Upload\Settings;

class Start {

    static private function start() {
        Settings::set(__CLASS__, time());
    }

    static public function stop() {
        Settings::clear(__CLASS__);
    }

}
