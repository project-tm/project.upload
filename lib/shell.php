<?php

namespace Project\Upload;

class Shell {

    const SH = '/usr/bin/php5 -c /opt/php70/etc/php-fpm.d/z-tyre.ru.conf -f ';

    static public function run($file) {
        pre($file);
        echo shell_exec(self::SH . $file);
    }

}
