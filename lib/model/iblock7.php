<?php

namespace Project\Upload\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class Iblock7Table extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'b_iblock_element_prop_s7';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true,
                'column_name' => 'IBLOCK_ELEMENT_ID'
                    )),
            new Main\Entity\StringField('CML2_ARTICLE', array(
                'column_name' => 'PROPERTY_163',
                    )),
            new Main\Entity\StringField('IMPORT_TYPE', array(
                'primary' => true,
                'column_name' => 'PROPERTY_196',
                    )),
            new Main\Entity\StringField('IMPORT_STATUS', array(
                'column_name' => 'PROPERTY_197',
                    )),
        );
    }

}
