<?php

namespace Project\Upload\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class Iblock6Table extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'b_iblock_element_prop_s6';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true,
                'column_name' => 'IBLOCK_ELEMENT_ID'
                    )),
            new Main\Entity\StringField('CML2_ARTICLE', array(
                'column_name' => 'PROPERTY_129',
                    )),
            new Main\Entity\StringField('IMPORT_TYPE', array(
                'primary' => true,
                'column_name' => 'PROPERTY_195',
                    )),
            new Main\Entity\StringField('IMPORT_STATUS', array(
                'column_name' => 'PROPERTY_200',
                    )),
        );
    }

}
