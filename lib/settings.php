<?php

namespace Project\Upload;

use Bitrix\Main\IO,
    Bitrix\Main\Application;

class Settings {

    static private function getPath($path = '') {
//        pre($path);
        $path = Application::getDocumentRoot() . '/upload/tmp/' . Config::MODULE . '/settings/' . ($path ? sha1($path) : '');
//        pre($path);
        CheckDirPath($path);
        return $path;
    }

    static public function get($path, $default = '') {
//        pre('get: '. $path);
        $path = self::getPath($path);
//        pre('get: '. $path);
        return file_exists($path) ? file_get_contents($path) : $default;
    }

    static public function clearDir() {
        IO\Directory::deleteDirectory(self::getPath());
    }

    static public function clear($path) {
        unlink(self::getPath($path));
    }

    static public function set($path, $data) {
        file_put_contents(self::getPath($path), $data);
    }

}
