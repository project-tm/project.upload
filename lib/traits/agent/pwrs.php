<?php

namespace Project\Upload\Traits\Agent;

use Project\Upload\Utility;

trait Pwrs {

    static public function saveCatalogData($arItem, $arData) {
        list($quality, $price) = self::parseCatalogData($arData);
//        Utility\Catalog::saveCatalog($arItem, false, $quality);
        Utility\Catalog::saveCatalog($arItem, false, $quality);
        if ($price) {
            Utility\Catalog::savePrice($arItem, $price, 'RUB');
        }
    }

    static public function parseCatalogData($arData) {
        $quality = $price = 0;
        foreach ($arData as $key => $value) {
            $isPrice = substr($key, 0, 5);
            $isRest = substr($key, 0, 4);
            if ($isPrice === 'price') {
                $type = explode('_', $key);
                if ($type[1] === 'mkrs') {
                    if (empty($price)) {
                        $price = $arData->$key;
//                        preExit($price, $arData);
                    }
                    if ($type[2] === 'rozn' and empty($price)) {
                        $price = $arData->$key;
                    }
                }
            } elseif ($isRest === 'rest') {
                $quality += $arData->$key;
            }
        }
        if(empty($price)) {
            foreach ($arData as $key => $value) {
                $isPrice = substr($key, 0, 5);
                if ($isPrice === 'price') {
                    $price = $arData->$key;
                }
            }
        }
//        pre($quality, $price);
        return array($quality, $price);
    }

}
