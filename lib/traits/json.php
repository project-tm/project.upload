<?php

namespace Project\Upload\Traits;

trait Json {

    static protected function getFile() {
        return self::upload(self::FILE);
    }

    static public function processPage($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
//        if ($page == 1) {
//            Records::clear();
//        }
//        pre($page);
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $filename = static::getFile();
        if (file_exists($filename)) {
            $json = json_decode(file_get_contents($filename));
            pre(count(static::parseJson($json)));
            foreach (static::parseJson($json) as $key => $data) {
                if ($key < $start) {
                    continue;
                }
                if ($key >= $end) {
                    return true;
                }
                foreach ($data as $key => $value) {
                    $data->{$key} = $value;
                }
                static::importData($data);
            }
        }
        return false;
    }

}
