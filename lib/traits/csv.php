<?php

namespace Project\Upload\Traits;

use SimpleXMLElement,
    cFile,
    Project\Import\Parse\Records,
    Project\Log,
    Project\Import\Data,
    Project\Import\Config;

trait Csv {

    static protected function getFile() {
        return self::upload(self::FILE);
    }

    static public function processPage($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
//        if ($page == 1) {
//            Records::clear();
//        }
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $filename = static::getFile();
        if (file_exists($filename)) {
            $filelen = strlen(file_get_contents($filename));
            $key = -1;
//            echo '<h3>Выполнено ' . round(($page - 1) / ceil(count($arData) / $limit) * 100, 2) . '% (' . $start . '/' . count($arData) . ')</h3>';
            echo '<h3>Разобрано ' . ($page - 1) * $limit . ' строк</h3>';
            set_time_limit(3600);
            if (($handle = fopen($filename, "r")) !== FALSE) {
//                pre($handle);
                while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
                    $key++;
                    if ($key < $start) {
                        continue;
                    }
                    if ($key >= $end) {
                        return true;
                    }
                    if (empty($key) or empty($data)) {
                        continue;
                    }
                    static::importData($data);
                };
                fclose($handle);
            }
        }
        return false;
    }

}
