<?php

namespace Project\Upload\Traits;

use Project\Upload\Helper,
    Project\Upload\Config,
    Project\Upload\Settings,
    Project\Tools\Utility;

trait Agent {

    static private $type = '';
    static private $page = 1;

    static protected function getType() {
        return self::$type;
    }

    static protected function cleateImport() {
        Helper\Sync::update(self::$type, 'clear');
    }

    static public function process($type) {
        self::$type = $type;
        self::$page = (int) Settings::get(__CLASS__, 1);
        pre(self::$type, self::$page);
        Utility\Report::add(Config::MODULE, self::$type, self::$page);
        if (self::$page == 1) {
            static::cleateImport();
        }
        if (static::processPage(self::$page)) {
            return self::next();
        } else {
            return self::stop();
        }
    }

    static protected function next() {
        Settings::set(__CLASS__, ++self::$page);
        return true;
    }

    static protected function stop() {
        Settings::clear(__CLASS__);
        return false;
    }

    static protected function getPath($path) {
        $path = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/' . Config::MODULE . '/agent/' . sha1($path);
        CheckDirPath($path);
        return $path;
    }

    static protected function upload($file) {
        $isCache = self::$page != 1;
        pre('$isCache', $isCache);
        if (empty($isCache)) {
            file_put_contents(self::getPath($file), file_get_contents($file));
        }
        return self::getPath($file);
    }

}
