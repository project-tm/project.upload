<?php

namespace Project\Upload\Agent\Sync;

use CIBlockElement,
    Bitrix\Main\Entity,
    Project\Tools,
    Project\Upload\Traits,
    Project\Upload\Model,
    Project\Upload\Config;

class Stop {

    use Traits\Agent;

    const LIMIT = Config::AGENT_LIMIT;

    static protected function cleateImport() {

    }

    static public function processPage() {
        $start = ($page - 1) * self::LIMIT;
        $end = ($page) * self::LIMIT;

        $param = array(
            'select' => array('ID', new Entity\ExpressionField('ID', 'SQL_CALC_FOUND_ROWS %s', 'ID')),
            'order' => array('ID' => 'ASC'),
            'filter' => array(
                '=IMPORT_TYPE' => array_keys(Config::getAgentList()),
                '=IMPORT_STATUS' => 'clear'
            ),
            'limit' => $limit,
            'offset' => $start
        );
        $pageIsNext = false;
        $el = new CIBlockElement;
        foreach (array(
    Config::CATALOG_TIRES_ID,
    Config::CATALOG_WHEELS_ID,
    Config::CATALOG_ACCUMULATORS_ID,
    Config::CATALOG_EXPANDABLES_ID
        ) as $iblockId) {
            $table = 'Project\Upload\Model\Iblock' . $iblockId . 'Table';
            $res = $table::getList($param);
            $count = $table::getEntity()->getConnection()->queryScalar('SELECT FOUND_ROWS() as TOTAL');
            $pageIsNext |= ($limit * $page) < $count;
            while ($arItem = $res->Fetch()) {
                $el->Update($arItem['ID'], array('ACTIVE' => 'N'));
                $table::update(array('ID' => $arItem['ID'],), array(
                    'IMPORT_STATUS' => ''
                ));
            }
        }
        return $pageIsNext;
    }

}
