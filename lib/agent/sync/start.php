<?php

namespace Project\Upload\Agent\Sync;

use Project\Upload\Traits,
    Project\Upload\Helper,
    Project\Upload\Config,
    Project\Upload\Settings;

class Start {

    use Traits\Agent;

    static protected function cleateImport() {
        Settings::clearDir();
    }

    static public function processPage() {
        foreach (Config::getAgentList() as $key => $value) {
            Helper\Sync::update($key, '');
        }
        return false;
    }

}
