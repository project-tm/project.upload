<?php

namespace Project\Upload\Agent\Pwrs;

use Project\Upload\Traits,
    Project\Upload\Config,
    Project\Upload\Utility,
    Project\Tools\Update;

class Tires {

    use Traits\Agent;
    use Traits\Section;
    use Traits\Json;
    use Traits\Agent\Pwrs;

    const LIMIT = Config::AGENT_LIMIT;
    const IBLOCK_ID = Config::CATALOG_TIRES_ID;
    const FILE = 'https://b2b.pwrs.ru/export_data/M13820.json';

    static protected function parseJson($json) {
        return $json->tires;
    }

    static protected function importData($arData) {
//        pre($arData);
        $arFields = array(
            'DATE_ACTIVE_FROM' => ConvertTimeStamp(time(), 'FULL'),
//            'TIMESTAMP_X' => ConvertTimeStamp(time(), 'FULL'),
//            'DATE_CREATE' => ConvertTimeStamp(time(), 'FULL'),
            'IBLOCK_ID' => self::IBLOCK_ID,
            'IBLOCK_SECTION_ID' => self::getSection(self::IBLOCK_ID, $arData->brand, $arData->model),
            'NAME' => $arData->name,
            'SORT' => '500',
            'ACTIVE' => 'Y',
            'CODE' => Update\Iblock::translit($arData->cae),
            'DETAIL_TEXT' => '',
            'DETAIL_TEXT_TYPE' => 'html',
            'PREVIEW_TEXT' => '',
            'PREVIEW_TEXT_TYPE' => 'html',
        );
        $propFields = array(
            'VYSOTA_PROFILYA' => $arData->height,
            'INDEKS_NAGRUZKI' => $arData->load_index,
            'INDEKS_SKOROSTI' => $arData->speed_index,
            'PROIZVODITEL' => $arData->brand,
            'CML2_ARTICLE' => $arData->cae,
            'MODEL_AVTOSHINY' => $arData->model,
            'SEZONNOST' => Utility\Property::props(self::IBLOCK_ID, 67, $arData->season),
            'TIP_AVTOSHINY' => $arData->tiretype,
            'KONSTRUKTSIYA_AVTOSHINY' => $arData->design,
            'SHIPY' => $arData->thorn_type ? 19 : '',
            'IMPORT_TYPE' => self::getType(),
            'IMPORT_STATUS' => '',
        );

        $arFiter = array(
            'IBLOCK_ID' => $arFields['IBLOCK_ID'],
//            'SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
            'PROPERTY_CML2_ARTICLE' => $propFields['CML2_ARTICLE'],
        );
        $arItem = Utility\Catalog::searchByFilter($arFiter, $arFields, $propFields);

        $arFields = $propFields = array();
        if ($arData->img_big_pish) {
            $img = $arData->img_big_pish;
            if (empty($arItem['DETAIL_PICTURE']) and ! empty($img)) {
                $domen = 'http://4tochki.ru/';
                $img = substr($img, strlen($domen));
                if ($arFile = Utility\Image::upload($img, $domen)) {
                    $arFields["DETAIL_PICTURE"] = $arFile;
                }
            }
        }

        Utility\Catalog::update($arItem, $arFields, $propFields);
        self::saveCatalogData($arItem, $arData);
//        if($quality) {
//            ImportTable::add(array(
//                'TYPE' => __CLASS__,
////                'PAGE' => $type,
//                'IBLOCK_ID' => self::IBLOCK_ID,
//                'CODE' => $arItem['ID'],
//                'QUANTITY' => $quality,
//            ));
//        }
//        preExit($arItem, $arFields, $propFields);
    }

}
