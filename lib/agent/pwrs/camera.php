<?php

namespace Project\Upload\Agent\Pwrs;

use Project\Upload\Traits,
    Project\Upload\Config,
    Project\Upload\Utility,
    Project\Tools\Update;

class Camera {

    use Traits\Agent;
    use Traits\Section;
    use Traits\Json;
    use Traits\Agent\Pwrs;

    const LIMIT = Config::AGENT_LIMIT;
    const IBLOCK_ID = Config::CATALOG_EXPANDABLES_ID;
    const FILE = 'https://b2b.pwrs.ru/export_data/M13844.json';

    static protected function parseJson($json) {
        return $json->camera;
    }

    static protected function importData($arData) {
//        preExit($arData);
        $arFields = array(
            'DATE_ACTIVE_FROM' => ConvertTimeStamp(time(), 'FULL'),
//            'TIMESTAMP_X' => ConvertTimeStamp(time(), 'FULL'),
//            'DATE_CREATE' => ConvertTimeStamp(time(), 'FULL'),
            'IBLOCK_ID' => self::IBLOCK_ID,
//            'IBLOCK_SECTION_ID' => self::getSection(self::IBLOCK_ID, 'Крепежи', $arData->sub_type, $arData->brand),
            'IBLOCK_SECTION_ID' => self::getSection(self::IBLOCK_ID, 'Камеры', $arData->brand),
            'NAME' => $arData->name,
            'SORT' => '500',
            'ACTIVE' => 'Y',
            'CODE' => Update\Iblock::translit($arData->cae),
            'DETAIL_TEXT' => '',
            'DETAIL_TEXT_TYPE' => 'html',
            'PREVIEW_TEXT' => '',
            'PREVIEW_TEXT_TYPE' => 'html',
        );
        $propFields = array(
            'PROIZVODITEL' => $arData->brand,
            'CML2_ARTICLE' => $arData->cae,
            'IMPORT_TYPE' => self::getType(),
            'IMPORT_STATUS' => '',
        );

        $arFiter = array(
            'IBLOCK_ID' => $arFields['IBLOCK_ID'],
//            'SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
            'PROPERTY_CML2_ARTICLE' => $propFields['CML2_ARTICLE'],
        );
//        pre($arFiter, $arFields, $propFields);
        $arItem = Utility\Catalog::searchByFilter($arFiter, $arFields, $propFields);

//        preExit($arItem);
        $arFields = $propFields = array();
        if ($arData->img_big_pish) {
            $img = $arData->img_big_pish;
            if (empty($arItem['DETAIL_PICTURE']) and ! empty($img)) {
                $domen = 'http://4tochki.ru/';
                $img = substr($img, strlen($domen));
                if ($arFile = Utility\Image::upload($img, $domen)) {
                    $arFields["DETAIL_PICTURE"] = $arFile;
                }
            }
        }

        Utility\Catalog::update($arItem, $arFields, $propFields);
        self::saveCatalogData($arItem, $arData);
//        if($quality) {
//            ImportTable::add(array(
//                'TYPE' => __CLASS__,
////                'PAGE' => $type,
//                'IBLOCK_ID' => self::IBLOCK_ID,
//                'CODE' => $arItem['ID'],
//                'QUANTITY' => $quality,
//            ));
//        }
//        preExit($arItem, $arFields, $propFields);
    }

}
