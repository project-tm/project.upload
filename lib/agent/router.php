<?php

namespace Project\Upload\Agent;

use Project\Upload\Config,
    Project\Upload\Settings,
    Project\Upload\Helper,
    Project\Tools\Utility;

class Router {

    static public function process() {
        try {
//            preDebug($key, date('H:i:s'));
//            preTrace();
            set_time_limit(0);
            if (Helper\Runtime::isStart()) {
                $agentList = Config::getAgentList();
                Utility\Report::setPath(Config::MODULE, '/test/result.txt');
                Utility\Report::clear(Config::MODULE);
                Utility\Report::add(Config::MODULE, date('H:i:s'));
                foreach ($agentList as $key => $value) {
                    pre($key, date('H:i:s', Settings::get($key)));
                    Utility\Report::add(Config::MODULE, $key, date('H:i:s', Settings::get($key)));
                }
                $type = Settings::get(__CLASS__);
                if ($type and empty($agentList[$type])) {
                    $type = false;
                }
                if (empty($type)) {
                    foreach ($agentList as $key => $value) {
                        $time = Settings::get($key);
                        if ($time and $time + ( (empty($value['time'])) ? Config::AGENT_PERIOD : $value['time']) > time()) {
                            continue;
                        }
                        $type = $key;
                        break;
                    }
                }
//                preExit(__LINE__, $type);
                if ($type) {
                    $agent = $agentList[$type];
                    if ($agent::process($type)) {
                        Settings::set(__CLASS__, $agent);
//                        preExit();
                    } else {
                        Settings::set($type, time());
                        Settings::clear(__CLASS__);
//                        preExit();
                    }
                }
                Helper\Runtime::stop();
                Utility\Report::add(date('H:i:s'));
            }
        } catch (Exception $ex) {
            echo ($ex->getMessage());
            pre($exc->getTraceAsString());
        }
        return '\Project\Upload\Agent::router();';
    }

}
