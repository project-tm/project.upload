<?php

namespace Project\Upload\Agent;

use Project\Upload\Traits,
    Project\Upload\Settings,
    Project\Upload\Config;

class Pwrs {

    static private $sheet = array(
        'Шины (Москва)' => Config::CATALOG_TIRES_ID,
        'Шины (Склад 2)' => Config::CATALOG_TIRES_ID,
        'Шины (Склад 3)' => Config::CATALOG_TIRES_ID,
        'Шины (Склад 4)' => Config::CATALOG_TIRES_ID,
        'Камеры (Москва)' => Config::CATALOG_EXPANDABLES_ID,
        'Диски (Москва)' => Config::CATALOG_WHEELS_ID,
        'Диски (Склад 3)' => Config::CATALOG_WHEELS_ID,
        'Диски (Склад 4)' => Config::CATALOG_WHEELS_ID,
        'Диски (Склад 8)' => Config::CATALOG_WHEELS_ID,
        'Крепежи (Москва)' => Config::CATALOG_EXPANDABLES_ID,
    );

    const LIMIT = 5;

    use Traits\Agent;

    const FILE = 'https://b2b.pwrs.ru/export_data/M10239.xlsx';

    static public function processPage($page) {
        if (empty($page)) {
//            ImportTable::tableClearType(__CLASS__);
        }

        $file = self::upload(self::FILE, $page);
        pre($page);
//        exit;

        $callStartTime = microtime(true);
        preMemory();
        $objPHPExcel = \PHPExcel_IOFactory::load($file);
        preMemory(date('H:i:s', microtime(true) - $callStartTime));

        $list = $objPHPExcel->getSheetNames();
        if (empty($list[$page])) {
            return self::stop();
        }
        $pageName = $list[$page];
        if (empty(self::$sheet[$pageName])) {
            return self::next();
        }

        $objPHPExcel->setActiveSheetIndex($page);
        $sheetData = $objPHPExcel->getActiveSheet();
        unset($objPHPExcel);
        foreach ($sheetData->getRowIterator() as $key => $row) {
            $item = array();
            foreach ($row->getCellIterator() as $index => $cell) {
                if (empty($keys)) {
                    $item[$index] = $cell->getCalculatedValue();
                } else {
                    $item[$keys[$index]] = $cell->getCalculatedValue();
                }
            }
            if ($key == 1) {
                $keys = $item;
            } else {
                ImportTable::add(array(
                    'TYPE' => __CLASS__,
                    'PAGE' => $pageName,
                    'IBLOCK_ID' => self::$sheet[$pageName],
                    'CODE' => $item['CAI'],
                    'PRICE' => $item['Розница (руб.)'],
                    'QUANTITY' => self::parseQuality($item['Кол-во (шт.)']),
                ));
            }
        }

        preMemory();
        pre(date('H:i:s', microtime(true) - $callStartTime));
        return self::next();
    }

}
