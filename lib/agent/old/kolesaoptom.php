<?php

namespace Project\Upload\Agent;

use Cutil,
    Project\Upload\Traits,
    Project\Upload\Settings,
    Project\Upload\Config,
    Project\Upload\Utility;

class KolesaOptom {

    use Traits\Agent;
    use Traits\Section;
    use Traits\Csv;

    const LIMIT = 100;
    const IBLOCK_ID = Config::CATALOG_WHEELS_ID;
    const FILE = 'http://kolesa-optom.ru/stocks/1564-33';

    static protected function importData($arData) {
        $arParams = array("replace_space" => "-", "replace_other" => "-");
        $arFields = array(
            'DATE_ACTIVE_FROM' => ConvertTimeStamp(time(), 'FULL'),
            'TIMESTAMP_X' => ConvertTimeStamp(time(), 'FULL'),
            'DATE_CREATE' => ConvertTimeStamp(time(), 'FULL'),
            'IBLOCK_ID' => self::IBLOCK_ID,
            'IBLOCK_SECTION_ID' => self::getSection(self::IBLOCK_ID, $arData[2]),
            'NAME' =>  $arData[1],
            'SORT' => '500',
            'ACTIVE' => 'Y',
            'CODE' => Cutil::translit( $arData[3], "ru", $arParams),
            'DETAIL_TEXT' => '',
            'DETAIL_TEXT_TYPE' => 'html',
            'PREVIEW_TEXT' => '',
            'PREVIEW_TEXT_TYPE' => 'html',
        );
        $propFields = array(
            'CML2_ARTICLE' => $arData[3],
            'VYLET_DISKA' => $arData->et,
            'DIAMETR_STUPITSY' => $arData[6],
            'COUNT_OTVERSTIY' => $arData[4],
            'MODEL_DISKA' => $arData[7],
        'MEZHBOLTOVOE_RASSTOYANIE' => $arData[5],
            'POSADOCHNYY_DIAMETR_DISKA' => $arData->bolts_spacing,
            'PROIZVODITEL' => $arData[2]
        );

        $arFiter = array(
            'IBLOCK_ID' => $arFields['IBLOCK_ID'],
            'SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
            'PROPERTY_CML2_ARTICLE' => $propFields['CML2_ARTICLE'],
        );
        pre($arData, $arFields, $propFields, $arFiter);
        $arItem = Utility\Catalog::searchByFilter($arFiter, $arFields, $propFields);

        $arFields = $propFields = array();
//        if ($arData->img_big_pish) {
//            $img = $arData->img_big_pish;
//            if (empty($arItem['DETAIL_PICTURE']) and ! empty($img)) {
//                $domen = 'http://4tochki.ru/';
//                $img = substr($img, strlen($domen));
//                if ($arFile = Utility\Image::upload($img, $domen)) {
//                    $arFields["DETAIL_PICTURE"] = $arFile;
//                }
//            }
//        }

        Utility\Catalog::update($arItem, $arFields, $propFields);
        Utility\Catalog::saveCatalog($arItem, false, 100);
        Utility\Catalog::savePrice($arItem, $arData[15], 'RUB');
//        if($quality) {
//            ImportTable::add(array(
//                'TYPE' => __CLASS__,
////                'PAGE' => $type,
//                'IBLOCK_ID' => self::IBLOCK_ID,
//                'CODE' => $arItem['ID'],
//                'QUANTITY' => $quality,
//            ));
//        }
//        preExit($arItem, $arFields, $propFields);
    }

}
