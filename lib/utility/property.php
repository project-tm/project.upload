<?php

namespace Project\Upload\Utility;

use CIBlockPropertyEnum;

class Property {

    static $arResplace = array(
        'Зимняя' => 'Зимние',
        'Летняя' => 'Летние',
        'Зимняя' => 'Зимние',
        'Зимняя' => 'Зимние',
    );

    public static function props($iblock, $propertyId, $name) {
        static $arProps = array();
        if (empty($arProps[$iblock][$propertyId])) {
            $arFilter = array(
                'IBLOCK_ID' => $iblock,
                'PROPERTY_ID' => $propertyId,
            );
            $res = CIBlockPropertyEnum::GetList(array(), $arFilter);
            while ($arItem = $res->Fetch()) {
                $arProps[$iblock][$propertyId][$arItem['VALUE']] = $arItem['ID'];
            }
        }
        if(isset(self::$arResplace[$name])) {
            $name = self::$arResplace[$name];
        }
        if (empty($arProps[$iblock][$propertyId][$name])) {
            $ibpenum = new \CIBlockPropertyEnum;
            $arProps[$iblock][$propertyId][$name] = $ibpenum->Add(Array(
                'PROPERTY_ID' => $propertyId,
                'XML_ID' => $name,
                'VALUE' => $name
            ));
//            preExit($iblock, $propertyId, $name);
        }
        return $arProps[$iblock][$propertyId][$name];
    }

}
