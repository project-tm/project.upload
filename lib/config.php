<?php

namespace Project\Upload;

use Bitrix\Main\Loader,
    Apstydio\Ztyre\Iblock,
    Project\Tools;

Loader::includeModule('apstydio.ztyre');

class Config {

    const IS_DEBUG = true;
    const MODULE = 'project.upload';
    const CATALOG_TIRES_ID = Iblock\Config::CATALOG_TIRES_ID;
    const CATALOG_WHEELS_ID = Iblock\Config::CATALOG_WHEELS_ID;
    const CATALOG_ACCUMULATORS_ID = Iblock\Config::CATALOG_ACCUMULATORS_ID;
    const CATALOG_EXPANDABLES_ID = Iblock\Config::CATALOG_EXPANDABLES_ID;
    const PRICE_ID = 1;
    const AGENT_LIMIT = 500;
    const AGENT_TIMEOUT = 120;
    const AGENT_PERIOD = 2 * 60 * 60;
    const AGENT_TEST = array(
        'sync.start' => '\Project\Upload\Agent\Sync\Start',
        'pwrs.rims' => '\Project\Upload\Agent\Pwrs\Rims',
        'pwrs.tires' => '\Project\Upload\Agent\Pwrs\Tires',
        'pwrs.fastener' => '\Project\Upload\Agent\Pwrs\Fastener',
        'pwrs.camera' => '\Project\Upload\Agent\Pwrs\Camera',
//        'kolesaoptom'=>'\Project\Upload\Agent\KolesaOptom',
        'sync.end' => '\Project\Upload\Agent\Sync\Stop',
    );
    const AGENT = array(
        'sync.start' => '\Project\Upload\Agent\Sync\Start',
        'pwrs.rims' => '\Project\Upload\Agent\Pwrs\Rims',
        'pwrs.tires' => '\Project\Upload\Agent\Pwrs\Tires',
        'pwrs.fastener' => '\Project\Upload\Agent\Pwrs\Fastener',
        'pwrs.camera' => '\Project\Upload\Agent\Pwrs\Camera',
//        'kolesaoptom'=>'\Project\Upload\Agent\KolesaOptom',
        'sync.end' => '\Project\Upload\Agent\Sync\Stop',
    );

    static public function getAgentList() {
        return Tools\Config::uploadTest() ? self::AGENT_TEST : self::AGENT;
    }

}
