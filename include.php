<?php

use Bitrix\Main\Loader;

//Loader::includeModule('project.core');
Loader::includeModule('apstydio.ztyre');
Loader::includeModule('iblock');

include_once (__DIR__ . '/project.tools/include.php');
Project\Tools\Config::set(array(
    /* Отладка */
    'adminDebug' => [10],
    /* Кеширование */
    'isCacheDebug' => false,
    'isCache' => true,
    /* Каталог */
    'catalogPriceId' => 1
));