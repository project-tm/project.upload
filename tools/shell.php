<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (Bitrix\Main\Loader::includeModule('project.upload')) {
    Project\Upload\Shell::run(__DIR__ .'/cron.php');
}